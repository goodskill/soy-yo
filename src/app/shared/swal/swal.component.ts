import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-swal',
  templateUrl: './swal.component.html',
  styleUrls: ['./swal.component.css'],
})
export class SwalComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public loadSwal (Icon:any, Title:string, timer:number){
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: timer,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    });
    Toast.fire({
      icon: Icon,
      title: Title
    })
  }

}
