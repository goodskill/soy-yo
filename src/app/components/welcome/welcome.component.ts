import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SwalComponent } from 'src/app/shared/swal/swal.component';
import { ValidateNameComponent } from '../validate-name/validate-name.component';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css'],
  providers: [SwalComponent, ValidateNameComponent]
})
export class WelcomeComponent implements OnInit {
  welcomeForm:FormGroup;
  constructor(private swal2: SwalComponent, private router: Router, private validate: ValidateNameComponent) {
    this.welcomeForm = new FormGroup({
      name: new FormControl('', Validators.required),
    });
  }

  ngOnInit(): void {
    this.validate.validateName() ? this.router.navigate(['/home']) : false;
  }
  submit(){
    if(this.welcomeForm.valid){
      this.swal2.loadSwal('success', `Welcome ${this.welcomeForm.value.name}`, 2000);
      localStorage.setItem('name', this.welcomeForm.value.name)
      setTimeout(() => {
        this.router.navigate(['/home']);
      }, 2000);
    } else {
      this.swal2.loadSwal('error', 'Pls confirm your name!', 3000);
    }
  }

}
