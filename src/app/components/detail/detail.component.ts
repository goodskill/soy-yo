import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientsService } from 'src/app/services/clients.service';
import { FormGroup, FormControl } from '@angular/forms';
import Swal from 'sweetalert2';
import { SwalComponent } from 'src/app/shared/swal/swal.component';
import { ValidateNameComponent } from '../validate-name/validate-name.component';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css'],
  providers: [SwalComponent, ValidateNameComponent]
})
export class DetailComponent implements OnInit {
  ids: any;
  data = new Array;
  Storage: any;
  showSpinner = true;
  editForm:FormGroup;
  infoEdit:any;
  allData= new Array;
  howMuch : number = 10; //Numero a cambiar para traer la cantidad de información que se quiere
  constructor(private route: ActivatedRoute, private api: ClientsService, private router: Router, private swal2: SwalComponent, private validate: ValidateNameComponent) {
    this.editForm = new FormGroup({
      name: new FormControl(''),
      identificationNumber: new FormControl(''),
      expirationDate: new FormControl(''),
      contactName: new FormControl(''),
      contactMail: new FormControl(''),
      ipAddress: new FormControl(''),
      logo: new FormControl(''),
    });
  }

  ngOnInit(): void {
    this.validate.validateName() ? false : this.router.navigate(['/welcome']);
    this.Storage = localStorage.getItem('data');
    this.ids = this.route.snapshot.paramMap.get('ids');
    this.ids = this.ids.substring(0, this.ids.length - 1).split("-");  
    if (this.Storage) {
      this.allData = JSON.parse(this.Storage);
      this.validatedLoad();
      this.showSpinner = false;
      this.selectedData();
    } else {
      this.getInfo(this.howMuch);
    }
  }

  getInfo(ids:any){
    var i = 0;
    do {
      this.api.getEntity(i).subscribe(data => {
        if(Object.entries(data.data).length){
          this.allData.push(data.data);
        } else {
          this.swal2.loadSwal('error', `EntityId: ${i} ${data.message}`, 3000);
          ids = ids -1;
          var index = this.ids.findIndex((element:any) => element == i)
          this.ids.splice(index, 1);
        }
        if (this.allData.length == ids) {      
          this.selectedData();
          this.validatedLoad();
        }
      });
      i = i + 1;
    } while (i < ids);
  }
  
  orderDes(key:any){
    const keys = key;
    this.data.sort((a, b)=>{
      if (a[keys] < b[keys]) {
        return -1;
      }
      if(a[keys] > b[keys]) {
        return 1
      }
      return 0
    })
  }

  selectedData(){
    for (let index = 0; index < this.ids.length; index++) {
      var find = this.allData.find(element => element.entityId == this.ids[index]);
      this.data.push(find);
    }
  }

  orderAs(key:any){
    const keys = key;
    this.data.sort((a, b)=>{
      if (a[keys] < b[keys]) {
        return 1;
      }
      if(a[keys] > b[keys]) {
        return -1;
      }
      return 0
    })
  }

  validatedLoad(){
    if (this.data.length == this.ids.length) {
      this.showSpinner = false;
    }
  }
  edit(id:number){
    this.infoEdit = this.data.find(element => element.entityId == id);
    var clones = JSON.parse(JSON.stringify(this.infoEdit));
    delete clones['entityId'];
    delete clones['attributeValidator'];
    delete clones['domain'];
    this.editForm.setValue(clones); 
  }

  saveEdit(){
    this.editForm.value.entityId = this.infoEdit.entityId;
    this.editForm.value.attributeValidator = this.infoEdit.attributeValidator;
    this.editForm.value.domain = this.infoEdit.domain;
    const index  = this.data.findIndex(element => element.entityId == this.editForm.value.entityId);
    this.data[index] = this.editForm.value;
    this.infoEdit = null;
    for (let index = 0; index < this.ids.length; index++) {
      var indexall = this.allData.findIndex(element => element.entityId == this.ids[index])
      var indexdata = this.data.findIndex(element => element.entityId == this.ids[index])
      this.allData[indexall] = this.data[indexdata];
    }
    localStorage.setItem('data', JSON.stringify(this.allData));
    this.swal2.loadSwal("success", "Great!", 3000);
    this.closeModal();
  }
  validateInfo(){
    if (localStorage.getItem('data')) {
      return true
    } else {
      return false
    }
  }
  closeModal(){
    var cancel = document.getElementById('cancel-button');
    cancel?.click();
  }
  delete(id:number){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        );
        var indexAll = this.allData.findIndex((element:any) => element.entityId == id);
        var indexdata = this.data.findIndex((element:any) => element.entityId == id);
        this.allData.splice(indexAll, 1);
        this.data.splice(indexdata, 1);
        localStorage.setItem('data', JSON.stringify(this.allData));
      }
    })
  }
  goBack(){
    this.router.navigate(['/home']);
  }
}
