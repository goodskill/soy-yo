import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SwalComponent } from 'src/app/shared/swal/swal.component';

@Component({
  selector: 'app-singout',
  templateUrl: './singout.component.html',
  styleUrls: ['./singout.component.css'],
  providers: [SwalComponent]
})
export class SingoutComponent implements OnInit {

  constructor(private swal2: SwalComponent, private router: Router) { }

  ngOnInit(): void {
  }
  
  singOut(){
    localStorage.clear();
    this.swal2.loadSwal("success", "See u later :D", 2000);
    this.router.navigate(['/welcome']);
  }
}
