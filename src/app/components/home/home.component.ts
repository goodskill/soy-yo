import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClientsService } from 'src/app/services/clients.service';
import { SwalComponent } from 'src/app/shared/swal/swal.component';
import { ValidateNameComponent } from '../validate-name/validate-name.component';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [SwalComponent, ValidateNameComponent]
})
export class HomeComponent implements OnInit {
  data = new Array;
  showSpinner = true;
  searchInfo = "";
  Storage:any;
  selected = new Array;
  howMuch : number = 10; //Numero a cambiar para traer la cantidad de información que se quiere
  constructor(private api: ClientsService, private swal2: SwalComponent, private router: Router, private validate: ValidateNameComponent) {
    
  }

  ngOnInit(): void {
    this.validate.validateName() ? false : this.router.navigate(['/welcome']);
    this.Storage = localStorage.getItem('data');
    if(localStorage.getItem('data')){
      this.data = JSON.parse(this.Storage);
      this.showSpinner = false;
      this.confirmData();
      this.validatedLoad();
    } else {
      this.getInfo(this.howMuch);
    }
  }

  getInfo(ids:number){
    for (let index = 0; index < ids; index++) {
      this.api.getEntity(index+1).subscribe(data => {
        this.data.push(data.data);
        this.validatedLoad();
        if (index === 9) {
          this.confirmData();
        }
      }, error => {
        this.swal2.loadSwal('error', error, 2000);
      })
    }
  }

  confirmData(){
    this.swal2.loadSwal('success', 'All ur data', 2000);
  }

  addInfo(id:number){
    if(this.selected.find(element => element == id)){
      var index = this.selected.indexOf(id);
      this.selected.splice(index, 1);      
    } else {
      this.selected.push(id);
    }
  }

  loadSwal(Icon?:any, Title?:any){
    this.swal2.loadSwal(Icon, Title, 2000);
  }

  validatedLoad(){
    if (this.data.length == 10) {
      this.showSpinner = false;
    }
  }
  
  continue(){
    this.selected.length > 0 ? this.goFor() : this.loadSwal("error", "Pls selected at least one");

  }
  goFor(){
    for (let index = 0; index < this.selected.length; index++) {
      this.searchInfo = this.searchInfo + this.selected[index] + "-";
}
this.router.navigate([`detail/${this.searchInfo}`]);
  }

}
