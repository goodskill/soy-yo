import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-validate-name',
  templateUrl: './validate-name.component.html',
  styleUrls: ['./validate-name.component.css']
})
export class ValidateNameComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    
  }
  public validateName(){
    if(localStorage.getItem('name')){
      return true;
    } else {
      return false;
    }
  }

}
