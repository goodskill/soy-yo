import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HttpClientModule,  } from '@angular/common/http';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { SwalComponent } from './shared/swal/swal.component';
import { DetailComponent } from './components/detail/detail.component';
import { NullPipe } from './pipes/null.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { SingoutComponent } from './components/singout/singout.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { ValidateNameComponent } from './components/validate-name/validate-name.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SpinnerComponent,
    SwalComponent,
    DetailComponent,
    NullPipe,
    SingoutComponent,
    WelcomeComponent,
    ValidateNameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
