import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ClientsService {
  constructor(private http: HttpClient) { }
  api = "https://f56c0ao48b.execute-api.us-east-1.amazonaws.com/dev/entity/v2.1/entities";


  public getEntity(entityId?:number): Observable<any>{
    return this.http.get<any>(`${this.api}/${entityId}`);
  }
}
