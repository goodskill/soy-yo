import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'null'
})
export class NullPipe implements PipeTransform {

  transform(value: any, value2?:any): any {
    if(value === '' || value === undefined || value === null) {
      return "No Info";
    } else {
      return value;
    }
    
  }

}
