# SoyYo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.0.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Instrucciones básicas dejadas por Eduardo Cortes

El programa cuenta con una validación donde se debe ingresar el nombre al inicio, si no, no te dejará continuar, y si estás "loggeado" no te dejará volver a escribir tu nombre
Por otro lado, en las instrucciones decían que debían ser 10 consultas de registros, es decir del 1 al 10, dentro de los dos componentes principales (detal y home), hay una variable global que debe ser modificada por si se desean menos o más registros (Ojo que deben ser numeros iguales si no el programa creará un bug al tratar de validar dos arrays distintos y puede obviar información).
Al no tener contacto con un backend o una base de datos, la información es tratada junto con el local storage, al darle cerrar sesión esta informacion será borrada y te pedirá ingresar tu nombre y empezar nuevamente
Los cambios en los datos que se muestran en las tablas son netamente del frontend como se indicaba en la guía.
La prueba fue desarrollada con éxito y completa al 100% con todos sus puntos
Solo se presentó dificultad con el servicio, ya que no trae la data en 1 sola consulta si no que toca iterar x veces según se necesite
Eso serían todos los puntos a aclarar
Muchas gracias
Eduardo Cortes
FrontEnd Developer

